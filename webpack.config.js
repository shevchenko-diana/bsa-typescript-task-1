const path = require('path');

module.exports = {
    entry: [ './index.ts'],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                '@babel/preset-env',
                                '@babel/preset-react'
                            ],
                            plugins: [
                                '@babel/transform-runtime'
                            ],
                            configFile: "./babel.config.js",
                            cacheDirectory: true,

                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    // style-loader
                    {loader: 'style-loader'},
                    // css-loader
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true
                        }
                    },
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    mode: 'development',
    devServer: {
        inline: true
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    }
};
