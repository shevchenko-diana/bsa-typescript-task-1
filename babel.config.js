const presets = ["@babel/react", "@babel/typescript", ["@babel/env", { "modules": false }]];

const plugins = ["@babel/plugin-proposal-class-properties", "@babel/plugin-syntax-jsx"];

module.exports = {
    presets,
    plugins
};
