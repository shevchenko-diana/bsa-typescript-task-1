export type ICreateElement = {
    tagName: string,
    className?: string,
    attributes: { [key: string]: string; }
}

export function createElement(elem: ICreateElement) {
    const element = document.createElement(elem.tagName);

    if (elem.className) {
        const classNames = elem.className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(elem.attributes).forEach((key) => element.setAttribute(key, elem.attributes[key]));

    return element;
}
