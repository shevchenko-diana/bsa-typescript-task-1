import {callApi} from '../helpers/apiHelper';
import {IFighter} from "../components/fighter";

class FighterService {
    async getFighters(): Promise<IFighter[]> {
        try {
            const endpoint = 'fighters.json';
            const apiResult = await callApi(endpoint, 'GET');

            return (apiResult as IFighter[]);
        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(id: number): Promise<IFighter> {

        try {
            const endpoint = `details/fighter/${id}.json`;
            const apiResult = await callApi(endpoint, 'GET');
            return (apiResult as IFighter);
        } catch (error) {
            throw error;
        }
    }
}

export const fighterService = new FighterService();
