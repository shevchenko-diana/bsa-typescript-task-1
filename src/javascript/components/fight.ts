import {controls} from '../../constants/controls';
import {IFighter, Positions} from './fighter'

const NEXT_ATTACK_WAIT = 10;

export async function fight(firstFighter: IFighter, secondFighter: IFighter): Promise<IFighter> {
    return new Promise((resolve) => {
            let keysPressed = new Map();
            let firstFighterHealth = firstFighter.health;
            let secondFighterHealth = secondFighter.health;

            let time1 = new Date();
            let time2 = new Date();

            function resolveDamage(health: number, dmg: number, attacker: IFighter) {
                health = health - dmg;
                if (health <= 0) {
                    resolve(attacker);
                }
                return health;
            }

            let damage;
            window.addEventListener('keyup', (event: KeyboardEvent) => {
                keysPressed.delete(event.code);
            });

            const areKeysPressed = (key: string) => keysPressed.get(key) === true

            window.addEventListener('keydown', function (event) {
                keysPressed.set(event.code, true);

                let now = new Date();

                if (controls.PlayerOneCriticalHitCombination.every(areKeysPressed)) {
                    //can't hit yet
                    if (time1.valueOf() > now.valueOf()) {
                        return;
                    }

                    time1 = now;
                    time1.setSeconds(time1.getSeconds() + NEXT_ATTACK_WAIT);

                    secondFighterHealth = resolveDamage(secondFighterHealth, firstFighter.attack * 2, firstFighter);
                    updateHealthBar('right', (secondFighterHealth / secondFighter.health));

                }

                if (controls.PlayerTwoCriticalHitCombination.every(areKeysPressed)) {
                    //can't hit yet
                    if (time2.valueOf() > now.valueOf()) {
                        return;
                    }

                    time2 = now;
                    time2.setSeconds(time2.getSeconds() + NEXT_ATTACK_WAIT);

                    firstFighterHealth = resolveDamage(firstFighterHealth, secondFighter.attack * 2, secondFighter);
                    updateHealthBar('left', (firstFighterHealth / firstFighter.health));
                }

                if (keysPressed.get(controls.PlayerOneAttack)) {
                    if (keysPressed.get(controls.PlayerOneBlock)) {
                        return;
                    }
                    damage = getDamage(firstFighter, keysPressed.get(controls.PlayerTwoBlock) ? secondFighter : {});
                    secondFighterHealth = resolveDamage(secondFighterHealth, damage, firstFighter);

                    updateHealthBar('right', (secondFighterHealth / secondFighter.health));

                }

                if (keysPressed.get(controls.PlayerTwoAttack)) {
                    if (keysPressed.get(controls.PlayerTwoBlock)) {
                        return;
                    }
                    damage = getDamage(secondFighter, keysPressed.get(controls.PlayerOneBlock) ? firstFighter : {});
                    firstFighterHealth = resolveDamage(firstFighterHealth, damage, secondFighter);
                    updateHealthBar('left', (firstFighterHealth / firstFighter.health));

                }
            });
        }
    );
}

async function updateHealthBar(position: Positions, percentage: number) {

    if (percentage == 1) {
        return;
    }
    let healthBar = (document.getElementById(`${position}-fighter-indicator`) as HTMLElement);
    healthBar.style.width = percentage * 100 + '%';
    healthBar.style.backgroundColor = 'hsl(' + 52 * percentage + ', 78.5%, 63.5%)';
}

export function getDamage(attacker: IFighter, defender: IFighter | {}) {
    // getHitPower - getBlockPower
    let attack = getHitPower(attacker);

    if (!Object.keys(defender).length) {
        return attack;
    }

    let block = getBlockPower(defender as IFighter);
    let damage = attack - block;

    return Math.max(0, damage);
}

export function getHitPower(fighter: IFighter) {
    return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter: IFighter) {
    return fighter.defense * randomNum();
}

function randomNum(): number {
    return Math.random() + 1;

}
