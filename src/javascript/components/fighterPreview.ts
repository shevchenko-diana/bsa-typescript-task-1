import {createElement} from '../helpers/domHelper';
import {controls} from '../../constants/controls';
import {IFighter, Positions} from './fighter'

export function createFighterPreview(fighter: IFighter, position: Positions) {
    const fighterPosition = position === 'right';
    const positionClassName = fighterPosition ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        attributes: {},
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`
    });


    if (!fighter) {
        return fighterElement;
    }

    fighterElement.appendChild(createFighterImage(fighter));

    let info = createElement({attributes: {}, tagName: 'div', className: 'fighter-preview___info'});

    let infoHeader = createElement({attributes: {}, tagName: 'h3', className: ''});
    infoHeader.innerText = 'Player info';
    info.appendChild(infoHeader);

    let infoTable = createTableFromRows({
        'Name': fighter.name,
        'Health': fighter.health.toString(),
        'Attack': fighter.attack.toString(),
        'Defense': fighter.defense.toString()
    });

    info.appendChild(infoTable);


    let keysHeader = createElement({attributes: {}, tagName: 'h3', className: ''});
    keysHeader.innerText = 'Keys';
    info.appendChild(keysHeader);

    let keysTable = createTableFromRows({
        'Attack': fighterPosition ? controls.PlayerTwoAttack : controls.PlayerOneAttack,
        'Block': fighterPosition ? controls.PlayerTwoBlock : controls.PlayerOneBlock,
        'Critical Hit': fighterPosition ?
            controls.PlayerTwoCriticalHitCombination.join('+') :
            controls.PlayerOneCriticalHitCombination.join('+')
    });
    info.appendChild(keysTable);


    fighterElement.appendChild(info);

    return fighterElement;
}

function createTableFromRows(tableMap: Record<string, string >) {
    let table = createElement({attributes: {}, tagName: 'table', className: 'fighter-preview___info-table'});

    Object.keys(tableMap).forEach((key) =>
        table.appendChild(createTableRow(key, tableMap[key])));
    return table;
}

function createTableRow(key: string, name: string) {
    let tr = createElement({attributes: {}, tagName: 'tr', className: ''});

    tr.appendChild(createTableTd(key));
    tr.appendChild(createTableTd(name));

    return tr;
}

function createTableTd(name: string) {
    let nameEl = createElement({attributes: {}, tagName: 'td', className: ''});
    nameEl.innerText = name;
    return nameEl;
}

export function createFighterImage(fighter: IFighter) {
    const {source, name} = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes
    });

    return imgElement;
}
