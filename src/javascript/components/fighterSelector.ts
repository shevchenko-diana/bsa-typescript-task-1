import {createElement} from '../helpers/domHelper';
import {renderArena} from './arena';
import versusImg from '../../../resources/versus.png';
import {createFighterPreview} from './fighterPreview';
import {fighterService} from '../services/fightersService';
import {IFighter} from './fighter'

function coalescin(val: any): boolean {
    return val !== null && val !== undefined
}

export function createFightersSelector() {
    let selectedFighters: IFighter[] = [];

    return async (event: Event, fighterId: number) => {

        const fighter = await getFighterInfo(fighterId);
        const [playerOne, playerTwo] = selectedFighters;
        const firstFighter = coalescin(playerOne) ? playerOne : fighter;
        const secondFighter = Boolean(playerOne) ? coalescin(playerTwo) ? playerTwo : fighter : playerTwo;
        selectedFighters = [firstFighter, secondFighter];

        renderSelectedFighters(selectedFighters);
    };
}

const fighterDetailsMap: Record<number, IFighter> = {};

export async function getFighterInfo(fighterId: number): Promise<IFighter> {
    if (!fighterDetailsMap[fighterId] !== null) {
        //todo check
        // if (!fighterDetailsMap.has(fighterId)) {
        return await fighterService.getFighterDetails(fighterId);

    } else {
        return fighterDetailsMap[fighterId];
    }
    // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
}

function renderSelectedFighters(selectedFighters: IFighter[]) {
    const fightersPreview = (document.querySelector('.preview-container___root') as Element);
    const [playerOne, playerTwo] = selectedFighters;
    const firstPreview = createFighterPreview(playerOne, 'left');
    const secondPreview = createFighterPreview(playerTwo, 'right');
    const versusBlock = createVersusBlock(selectedFighters);

    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighter[]) {
    const canStartFight = selectedFighters.filter(Boolean).length === 2;
    const onClick = () => startFight(selectedFighters);
    const container = createElement({attributes: {}, tagName: 'div', className: 'preview-container___versus-block'});
    const image = createElement({
        tagName: 'img',
        className: 'preview-container___versus-img',
        attributes: {src: versusImg}
    });
    const disabledBtn = canStartFight ? '' : 'disabled';
    const fightBtn = createElement({
        attributes: {},
        tagName: 'button',
        className: `preview-container___fight-btn ${disabledBtn}`
    });

    fightBtn.addEventListener('click', onClick, false);
    fightBtn.innerText = 'Fight';
    container.append(image, fightBtn);

    return container;
}

function startFight(selectedFighters: IFighter[]) {
    renderArena(selectedFighters);
}
