import {createElement} from '../../helpers/domHelper';

export function showModal(title: string, bodyElement: Node, onClose = () => {
}) {
    const root = (getModalContainer() as HTMLElement);
    const modal = createModal(title, bodyElement, onClose);

    root.append(modal);
}

function getModalContainer() {
    return document.getElementById('root');
}

function createModal(title: string, bodyElement: Node, onClose: any) {
    const layer = createElement({attributes: {}, tagName: 'div', className: 'modal-layer'});
    const modalContainer = createElement({attributes: {}, tagName: 'div', className: 'modal-root'});
    const header = createHeader(title, onClose);

    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);

    return layer;
}

function createHeader(title: string, onClose: any) {
    const headerElement = createElement({attributes: {}, tagName: 'div', className: 'modal-header'});
    const titleElement = createElement({attributes: {}, tagName: 'span'});
    const closeButton = createElement({attributes: {}, tagName: 'div', className: 'close-btn'});

    titleElement.innerText = title;
    closeButton.innerText = '×';

    const close = () => {
        hideModal();
        onClose();
    }
    closeButton.addEventListener('click', close);
    headerElement.append(titleElement, closeButton);

    return headerElement;
}

function hideModal() {
    const modal = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
}
