import {showModal} from './modal';
import {createElement} from '../../helpers/domHelper';
import {createFighterImage} from '../fighterPreview';
import {IFighter} from "../fighter";


export function showWinnerModal(fighter: IFighter) {
    // call showModal function
    let bodyElem = createElement({attributes: {}, tagName: 'div', className: 'modal-body-info'});
    bodyElem.appendChild(createFighterImage(fighter));

    let someText = createElement({attributes: {}, tagName: 'p'});
    someText.innerText = 'To start a new game - refresh this page or hit Refresh';

    bodyElem.appendChild(someText);

    const refreshButton = createElement({attributes: {}, tagName: 'div', className: 'close-btn'});

    refreshButton.innerText = 'Refresh';

    refreshButton.addEventListener('click', location.reload.bind(location));
    bodyElem.append(refreshButton);

    showModal(
        `${fighter.name} won!`,
        bodyElem,
    );
}
